/*
 * Pokedu-Go
 * Game-based Learning with Augmented Reality
 * http://pokedugo.faisalman.com/
 * Copyright (c) Faisal Salman 2017
 */

// libraries
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var express = require('express');
var path = require('path');
var session = require('express-session');

//config
var config = {
    secret : 'unitedbyhcl'
};

// app
var app = express();
var env = process.env.NODE_ENV || 'development';
var hbs = exphbs.create({
    defaultLayout   : 'main',
    extname         : '.hbs',
    partialsDir     : 'views/partials/'
});
app.engine('hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('x-powered-by', false);
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret  : config.secret,
    cookie  : { maxAge: 9999999 },
    saveUninitialized : false,
    resave  : false
}));

app.get('/', function (req, res) {
  res.render('index', {
      layout : 'index'
  });
});

app.get('/help', function (req, res) {
  res.render('help');
});

app.get('/pokedux', function (req, res) {
  res.render('pokedux');
});

app.get('/user', function (req, res) {
  res.render('user');
});

app.get('/menu', function (req, res) {
  res.render('levels');
});

app.get('/play', function (req, res) {
  res.render('play');
});

// error 404
app.use(function (req, res, next) {
    res.status(404).render('pesan', {
        title   : 'Oops',
        pesan   : {
            icon        : 'bug',
            judul       : 'Error [404]',
            judul       : 'Error 404',
            deskripsi   : 'Page not found.'
        }
    });
});

// error 500
app.use(function(err, req, res, next) {
    console.log(err);
    res.status(500).render('pesan', {
        title   : 'Oops',
        pesan   : {
            icon        : 'bug',
            judul       : 'Error [500]',
            deskripsi   : 'Server error. Please try again later.'
        }
    });
});

/**************
 * run server *
 **************/

app.listen(8060, function () {
  console.log('Example app listening on port 8060!');
});

module.exports = app;